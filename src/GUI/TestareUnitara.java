package GUI;


import static org.junit.Assert.*;

import org.junit.Test;

import DesignByContract.Bank;
import model.Person;
import model.SavingAccount;

public class TestareUnitara 
{
	@Test
	public void Testing()
	{
		Bank b = new Bank();
		b.addPerson(0, new Person("Bizau Diana", 12));
		b.addSavingAccount(0,  new SavingAccount(new Person("Alexandra Hancu", 23), 2000, 23, "saving"));
		assertEquals(2000, b.getSold(0));
	}
}

