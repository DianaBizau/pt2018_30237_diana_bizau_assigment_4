package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.Serializable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import DesignByContract.Bank;
import DesignByContract.BankDeserialization;
import DesignByContract.BankSerialization;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

public class Interface implements Serializable
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Bank b;
	private Bank b1;
	private JFrame f, f1, f2;
	private JPanel panel;
	private JButton bc1, bc2, bc3, ba1, ba2, ba3, ba, bs;
	private JLabel l1, l2, lc1, lc2, la1, la2, la3, la4, lID, lID2;
	private DefaultTableModel model1, model2;
	private JScrollPane pane1, pane2;
	private JTable table1, table2;
	private JTextArea ta1, ta2, tac1, tac2, tac3, tac4, tID, tID2;
	BankSerialization bb;
	BankDeserialization bd;
	
	public Interface(Bank b)
	{
		this.b = b;
		frameCreation();
		labelCreation();
		buttonCreation();
		textAreaCreation();
		panelCreation();
		clientsInt();
		accountsInt();
		b1 = new Bank();
		bb = new BankSerialization(b1);
		frameVisiblity();

	}
	
	public void frameCreation()
	{
		// interfata principala
		f = new JFrame("Bank");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(780, 700);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int x1 = (int) ((d.getWidth() - f.getWidth()) / 2);
		int y1 = (int) ((d.getHeight() - f.getHeight()) / 2);
		f.setLocation(x1, y1); // pentru a face ca interfata sa se deschida la mijloc
		
		// interfata pentru tabelul de persoane
		f1 = new JFrame("Clienti");
		f1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    f1.setSize(600, 350);
		int x2 = (int) ((d.getWidth() - f.getWidth()) / 8);
		int y2 = (int) ((d.getHeight() - f.getHeight()) / 8);
		f1.setLocation(x2, y2); // pentru a face ca interfata sa se deschida la mijloc
			
		// interfata pentru tabelul de conturi
		f2 = new JFrame("Conturi");
		f2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f2.setSize(600, 350);
		int x3 = (int) ((d.getWidth() - f.getWidth()) / 11);
		int y3 = (int) ((d.getHeight() - f.getHeight()) / 11);
		f2.setLocation(x3, y3); // pentru a face ca interfata sa se deschida la mijloc
	}
	
	public void labelCreation()
	{
		l1 = new JLabel("CLIENTI: ");
		l1.setForeground(Color.black);
		l1.setFont(new Font("CAMBRIA", Font.BOLD, 20));
		l1.setBounds(100, 170, 150, 50);
		
		l2 = new JLabel("CONTURI: ");
		l2.setForeground(Color.black);
		l2.setFont(new Font("CAMBRIA", Font.BOLD, 20));
		l2.setBounds(100, 540, 175, 50);
		
		lc1 = new JLabel("Introduceti numele: ");
		lc1.setBounds(200, 50, 180, 50);
		lc1.setFont(new Font("CAMBRIA", Font.BOLD, 16));
		
		lc2 = new JLabel("Introduceti nr de cont: ");
		lc2.setBounds(200, 100, 180, 50);
		lc2.setFont(new Font("CAMBRIA", Font.BOLD, 16));
		
		la1 = new JLabel("Introduceti numele: ");
		la1.setBounds(200, 300, 160, 50);
		la1.setFont(new Font("CAMBRIA", Font.BOLD, 16));
		
		la2 = new JLabel("Introduceti nr de cont: ");
		la2.setBounds(200, 350, 180, 50);
		la2.setFont(new Font("CAMBRIA", Font.BOLD, 16));
		
		la3 = new JLabel("Introduceti sold-ul: ");
		la3.setBounds(200, 400, 150, 50);
		la3.setFont(new Font("CAMBRIA", Font.BOLD, 16));
		
		la4 = new JLabel("Introduceti tip-ul: ");
		la4.setBounds(200, 450, 160, 50);
		la4.setFont(new Font("CAMBRIA", Font.BOLD, 16));
		
		lID = new JLabel("ID: ");
		lID.setBounds(200, 0, 150, 50);
		lID.setFont(new Font("CAMBRIA", Font.BOLD, 16));
		
		lID2 = new JLabel("ID: ");
		lID2.setBounds(200,  250, 150,  50);
		lID2.setFont(new Font("CAMBRIA", Font.BOLD, 16));
	}
	
	public void buttonCreation()
	{
		bc1 = new JButton("Adaugare");
		bc1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int ID = Integer.parseInt(tID.getText());
				String nume = ta1.getText();
				int nrCont = Integer.parseInt(ta2.getText());
				b.addPerson(ID, new Person(nume, nrCont));
				model1.addRow(new Object[]{ID, nume, nrCont});
			
			}
		});
		bc1.setBounds(200, 160, 100, 60);
	
		bc2 = new JButton("Stergere");
		bc2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int ID = Integer.parseInt(tID.getText());

				try
				{
					int IDD = b.getIDtoDeleteClient(ID);
					model1.removeRow(IDD);
					model1.fireTableDataChanged();
				}
				catch(IOException e1)
				{
					e1.printStackTrace();
				}
			}
		});
		bc2.setBounds(300, 160, 100, 60);
		
		bc3 = new JButton("Afisare");
		bc3.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				bd = new BankDeserialization(b);
				f1.setVisible(true);
			}
		});
		bc3.setBounds(400, 160, 100, 60);
		
		ba1 = new JButton("Adaugare");
		ba1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int ID = Integer.parseInt(tID2.getText());
				String nume = tac1.getText();
				int nrCont = Integer.parseInt(tac2.getText());
				int sold = Integer.parseInt(tac3.getText());
				String tip = tac4.getText();
				String a1 = "spending";
				String a2 = "saving";
				
				try
				{
					b.AccountIDMismatch(ID);
					b.AccountType(nume, tip);
					if(tip.equals(a1)) 
						b.addSpendingAccount(ID, new SpendingAccount(new Person(nume, nrCont), sold, nrCont, tip));
					else if(tip.equals(a2)) 
						b.addSavingAccount(ID, new SavingAccount(new Person(nume, nrCont), sold, nrCont, tip));
					model2.addRow(new Object[]{ID, nume, sold, nrCont, tip} );
				}
				catch(IOException e1)
				{
					e1.printStackTrace();
				}
				
				
			}
		});
		ba1.setBounds(200, 535, 100, 60);
		
		ba2 = new JButton("Stergere");
		ba2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int ID = Integer.parseInt(tID2.getText());
				
				try
				{
					int IDD = b.getIDtoDeleteAccount(ID);
					model2.removeRow(IDD);
					model2.fireTableDataChanged();
				}
				catch(IOException e1)
				{
					e1.printStackTrace();
				}
			}
		});
		ba2.setBounds(300, 535, 100, 60);
		
		ba3 = new JButton("Afisare");
		ba3.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				bd = new BankDeserialization(b);
				f2.setVisible(true);
			}
		});
		ba3.setBounds(400, 535, 100, 60);
		
		ba = new JButton("AdaugareB");
		ba.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int ID = Integer.parseInt(tID2.getText());
				String nume = tac1.getText();
				int nrCont = Integer.parseInt(tac2.getText());
				int sold = Integer.parseInt(tac3.getText());
				String tip = tac4.getText();
				int n_sold = sold + b.getSold(ID);
				String a1 = "spending";
				String a2 = "saving";
				
				if(tip.equals(a1)) 
					b.setSpendingSold(ID, new SpendingAccount(new Person(nume, nrCont), n_sold, nrCont, tip));
				else if(tip.equals(a2))
					b.setSavingSold(ID, new SavingAccount(new Person(nume, nrCont), n_sold, nrCont, tip));
				
				model2.setValueAt(ID, ID, 0);
				model2.setValueAt(nume, ID, 1);
				model2.setValueAt(n_sold, ID,  2);
				model2.setValueAt(nrCont, ID, 3);
				model2.setValueAt(tip, ID, 4);
			}
		});
		ba.setBounds(500,535, 100, 60);
		
		bs = new JButton("Retragere");
		bs.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int ID = Integer.parseInt(tID2.getText());
				String nume = tac1.getText();
				int nrCont = Integer.parseInt(tac2.getText());
				int sold = Integer.parseInt(tac3.getText());
				String tip = tac4.getText();
				int n_sold = b.getSold(ID)- sold ;
				String a1 = "spending";
				String a2 = "saving";
				
				if(tip.equals(a1)) 
					b.setSpendingSold(ID, new SpendingAccount(new Person(nume, nrCont), n_sold, nrCont, tip));
				else if(tip.equals(a2))
					b.setSavingSold(ID, new SavingAccount(new Person(nume, nrCont), n_sold, nrCont, tip));
				
				model2.setValueAt(ID, ID, 0);
				model2.setValueAt(nume, ID, 1);
				model2.setValueAt(n_sold, ID,  2);
				model2.setValueAt(nrCont, ID, 3);
				model2.setValueAt(tip, ID, 4);
			}
		});
		bs.setBounds(600, 535, 100, 60);
	}
	
	public void panelCreation()
	{
		panel = new JPanel();
		panel.setLayout(null);
		
		panel.add(l1);
		panel.add(l2);
		panel.add(lc1);
		panel.add(lc2);
		panel.add(la1);
		panel.add(la2);
		panel.add(la3);
		panel.add(la4);
		panel.add(lID);
		panel.add(lID2);
		
		panel.add(bc1);
		panel.add(bc2);
		panel.add(bc3);
		
		panel.add(ba1);
		panel.add(ba2);
		panel.add(ba3);
		
		panel.add(ba);
		panel.add(bs);
		
		panel.add(ta1);
		panel.add(ta2);
		panel.add(tac1);
		panel.add(tac2);
		panel.add(tac3);
		panel.add(tac4);
		panel.add(tID);
		panel.add(tID2);
				
		f.setContentPane(panel);

	}
	
	public void textAreaCreation()
	{
		ta1 = new JTextArea();
		ta1.setBounds(400, 65, 150, 20);
		
		ta2 = new JTextArea();
		ta2.setBounds(400, 115, 150, 20);
		
		tac1 = new JTextArea();
		tac1.setBounds(400, 315, 150, 20);

		tac2 = new JTextArea();
		tac2.setBounds(400, 365, 150, 20);

		tac3 = new JTextArea();
		tac3.setBounds(400, 415, 150, 20);

		tac4 = new JTextArea();
		tac4.setBounds(400, 465, 150, 20);
		
		tID = new JTextArea();
		tID.setBounds(400, 15, 150, 20);
		
		tID2 = new JTextArea();
		tID2.setBounds(400, 265, 150, 20);
	}
	
	public void clientsInt()
	{
		String[] coloane = new String[] { "ID", "Nume", "Numar de cont" } ;
		Object[][] data = new Object[][]{ { 0, "Alexandra Hancu", 32451234},
										  { 1, "Bizau Diana", 53712351},
										  { 2, "Popescu Cristina", 75312373},};
										 
	    Person p0 = new Person("Alexandra Hancu", 32451234);
	    Person p1 = new Person("Bizau Diana", 53712351);
	    Person p2 = new Person("Popescu Cristina", 75312373);
	    b.addPerson(0, p0);
	    b.addPerson(1, p1);
	    b.addPerson(2, p2);
	    model1 = new DefaultTableModel(data,coloane);
		table1 = new JTable(model1);
	    pane1 = new JScrollPane(table1);
	    f1.add(pane1);
	}
	
	public void accountsInt()
	{
		String[] coloane = new String[] {"ID", "Nume", "Sold", "Nr. Cont", "Tip" } ;
		Object[][] data = new Object[][]{ {0, "Bizau Diana", 25000, 10, "spending"},
										  {1, "Alexandra Hancu", 15000, 11, "saving"},
										  {2, "Coroian Cristiana", 20000, 12, "spending"},
										  {3, "Bizau Diana", 50000, 13, "saving"},
										  {4, "Popescu Cristina", 10000, 14, "spending"},};
		SpendingAccount a1 = new SpendingAccount(new Person("Bizau Diana", 10), 25000, 10, "spending");
		SavingAccount a2 = new SavingAccount(new Person("Alexandra Hancu", 11), 15000, 11, "saving");
		SpendingAccount a3 = new SpendingAccount(new Person("Coroian Cristiana", 12), 20000, 12, "spending");
		SavingAccount a4 = new SavingAccount(new Person("Bizau Diana", 13), 50000, 13, "saving");
		SpendingAccount a5 = new SpendingAccount(new Person("Popescu Cristina", 14), 10000, 14, "spending");
		b.addSpendingAccount(0, a1);
		b.addSavingAccount(1, a2);
		b.addSpendingAccount(2, a3);
		b.addSavingAccount(3, a4);
		b.addSpendingAccount(4, a5);
		
	    model2 = new DefaultTableModel(data,coloane);
		table2 = new JTable(model2);
	    pane2 = new JScrollPane(table2);
	    f2.add(pane2);
	}

		
	public void frameVisiblity()
	{
		f.setVisible(true);
	}
	
	
}

