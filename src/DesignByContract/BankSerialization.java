package DesignByContract;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class BankSerialization 
{
	public BankSerialization(Bank b)
	{
		try
		{
			FileOutputStream file = new FileOutputStream("serialization.ser");
			ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(b);
			out.close();
			file.close();
			System.out.print("Serializarea a fost cu succes.");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
