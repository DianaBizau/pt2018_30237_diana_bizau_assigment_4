package DesignByContract;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class BankDeserialization 
{
	public BankDeserialization(Bank b)
	{
		try
		{
			FileInputStream file = new FileInputStream("serialization.ser");
			ObjectInputStream in = new ObjectInputStream(file);
			b = (Bank) in.readObject();
			in.close();
			file.close();
		} 
		catch(IOException i)
		{
			i.printStackTrace();
			return;
		}
		catch(ClassNotFoundException c)
		{
			System.out.print("Banca nu exista.");
			c.printStackTrace();
			return;
		}	
	}
}
