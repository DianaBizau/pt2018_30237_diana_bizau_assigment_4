package DesignByContract;

import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

public interface BankProc 
{
	/**
	 * Adaugam o noua persoana in hashtable-ul de Persoane.
	 * Daca acesta exista deja in tabela, atunci el nu va fi adaugat, ci va fi aruncata
	 * o exceptie.
	 * 
	 * @param ID
	 * @param p
	 * @pre clients.getKey() == NULL
	 * @post clients.getKey() != NULL
	 */
	public void addPerson(int ID, Person p);
	
	/**
	 * Stergem o persoana din hastable-ul de clienti.
	 * Daca persoana cu acest ID nu exista in table, atunci va fi arucanta o exceptie
	 * care ne va spune ca aceasta persoana nu exista in tabela.
	 * @param ID
	 * @pre clients.getKey() != NULL
	 * @post clients.getKey() == NULL
	 */
	public void removePerson(int ID);
	
	/**
	 * Adaugam un nou cont de tipul SavingAccount in hashtable-ul de account-uri.
	 * Daca acesta exista deja in tabela, atunci el nu va fi adaugat, ci va fi aruncata
	 * o exceptie.
	 * @param ID
	 * @param a
	 * @pre accounts.getKey() == NULL
	 * @post accounts.getKey() != NULL
	 */
	public void addSavingAccount(int ID, SavingAccount a);
	
	/**
	 * Stergem un cont de tipul SavingAccount din hashtabel-ul de account-uri.
	 * Daca contul cu acest ID nu exista in table, atunci va fi arucanta o exceptie
	 * care ne va spune ca acest cont nu exista in tabela.
	 * @param ID
	 * @pre accounts.getKey() != NULL
	 * @post accounts.getKey() == NULL
	 */
	public void removeSavingAccount(int ID);
	
	/**
	 * Adaugam un nou cont de tipul SpendingAccount in hashtable-ul de account-uri.
	 * Daca acesta exista deja in tabela, atunci el nu va fi adaugat, ci va fi aruncata
	 * o exceptie.
	 * @param ID
	 * @param a
	 * @pre accounts.getKey() == NULL
	 * @post accounts.getKey() != NULL
	 */
	public void addSpendingAccount(int ID, SpendingAccount a);
	
	/**
	 * Stergem un cont de tipul SavingAccount din hashtabel-ul de account-uri.
	 * Daca contul cu acest ID nu exista in table, atunci va fi arucanta o exceptie
	 * care ne va spune ca acest cont nu exista in tabela.
	 * @param ID
	 * @pre accounts.getKey() != NULL
	 * @post accounts.getKey() == NULL
	 */
	public void removeSpendingAccount(int ID);
	
}

