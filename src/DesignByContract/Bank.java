package DesignByContract;

import java.util.List;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

import GUI.Interface;
import model.Account;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

public class Bank implements BankProc, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Hashtable<Integer, Person> clients;
	private Hashtable<Integer, Account> accounts;
	@SuppressWarnings("unused")
	private Interface i;
	
	public Bank()
	{
		clients = new Hashtable<Integer, Person>(1000);
		accounts = new Hashtable<Integer, Account>(1000);
	}
	
	public void addPerson(int ID, Person p)
	{
		assert !isAccount(ID) : "Contul exista deja";
		clients.put(ID, p);
	}
	
	public void removePerson(int ID)
	{
		assert isAccount(ID) : "Contul nu exista";
		clients.remove(ID);
	}

	public void addSavingAccount(int ID, SavingAccount a) 
	{
		assert !isAccount(ID) : "Contul exista deja";
		accounts.put(ID, a);
		a.addObserver(a.getPerson());
	}

	public void removeSavingAccount(int ID) 
	{
		assert isAccount(ID) : "Contul nu exista";
		accounts.remove(ID);
	}

	public void addSpendingAccount(int ID, SpendingAccount a) 
	{
		assert !isAccount(ID) : "Contul exista deja";
		accounts.put(ID, a);
		a.addObserver(a.getPerson());
	}

	public void removeSpendingAccount(int ID) 
	{
		assert isAccount(ID) : "Contul nu exista";
		accounts.remove(ID);
	}
	
	public void hashSortClients()
	{
		assert clients.size() < 1 : "Tabelul e gol";
		List<Integer> x = new ArrayList<Integer>(clients.keySet());
		Collections.sort(x);
		for(Integer b: x)
			System.out.println(b + " " + clients.get(b).getNume() + " " + clients.get(b).getNrCont());
	}
	
	public void hashSortAccounts()
	{
		assert accounts.size() < 1 : "Tabelul e gol";
		List<Integer> x = new ArrayList<Integer>(accounts.keySet());
		Collections.sort(x);
		for(Integer b: x)
			System.out.println(b + " " + accounts.get(b).getPerson().getNume() + " " + accounts.get(b).getPerson().getNrCont() + " " + accounts.get(b).getSold() + " " + accounts.get(b).getTip());
	}
	
	public int getIDtoDeleteClient(int ID) throws IOException
	{
		assert isAccount(ID) : "Clientul nu exista";
		int OK = 0;
		for(Map.Entry<Integer, Person> a: clients.entrySet())
			if(a.getKey() == ID) OK = 1;
		
		IOException e = null;
		if(OK == 0)
		{
			e = new IOException();
			throw e;
		}
		else return ID;
	}
	
	public void IDMismatch(int ID) throws IOException
	{
		assert isAccount(ID) : "Clientul nu exista";
		int OK = 1;
		for(Map.Entry<Integer, Person> a: clients.entrySet())
			if(a.getKey() == ID) OK = 0;
		
		IOException e = null;
		if(OK == 0)
		{
			e = new IOException();
			throw e;
		}
	}
	
	public void AccountIDMismatch(int ID) throws IOException
	{
		assert !isAccount(ID) : "Contul nu exista";
		int OK=1;
		for(Map.Entry<Integer, Account> a: accounts.entrySet())
			if(a.getKey() == ID) OK=0;
		
		IOException e = null;
		if(OK ==0)
		{
			e = new IOException();
			throw e;
		}
	}
	
	public int getIDtoDeleteAccount(int ID) throws IOException
	{
		assert isAccount(ID) : "Contul nu exista";
		int OK = 0;
		for(Map.Entry<Integer, Account> a: accounts.entrySet())
			if(a.getKey() == ID) OK = 1;
		
		IOException e = null;
		if(OK == 0)
		{
			e = new IOException();
			throw e;
		}
		else return ID;
	}
	
	public void AccountType(String nume, String tip) throws IOException
	{
		int OK=1;
		for(Map.Entry<Integer, Account> a: accounts.entrySet())
			if(a.getValue().getPerson().getNume() == nume && a.getValue().getTip().equals(tip)) 
				OK=0;
		
		IOException e = null;
		if(OK==0)
		{
			e = new IOException();
			throw e;
		}
	}
	
	public void setSavingSold(int ID, SavingAccount a)
	{
		accounts.put(ID, a);
		a.extract();
	}
	
	public void setSpendingSold(int ID, SpendingAccount a)
	{
		accounts.put(ID, a);
		a.extract();

	}
	
	public int getSold(int ID)
	{
		assert isAccount(ID) : "Contul nu exista";
		for(Map.Entry<Integer, Account> a: accounts.entrySet())
			if(a.getKey() == ID) return a.getValue().getSold();
		return 0;
	}
	
	
	public boolean isAccount(int ID)
	{
		int OK=0;
		for(Map.Entry<Integer, Account> a: accounts.entrySet())
			if(a.getKey() == ID) OK=1;
		
		if(OK == 0) 
			return false;
		return true;
	}
			
	public static void main(String[] args)
	{
		Bank b = new Bank();
		b.i = new Interface(b);
	}

}

