package model;

import java.io.Serializable;

public class SpendingAccount extends Account implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SpendingAccount(Person p, int sold, int nrCont, String tip)
	{
		super(p, sold, nrCont, tip);
	}

	public Person getPerson()
	{
		return this.p;
	}
	
	public int getSold()
	{
		return this.sold;
	}
	
	public String getTip()
	{
		return tip;
	}
	
	public void extract() 
	{
		setChanged();
		this.notifyObservers("Soldul a fost actualizat.");
	}
	
}

