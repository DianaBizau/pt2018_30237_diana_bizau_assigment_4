package model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Serializable, Observer
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nume;
	private int nrCont;
	
	public Person(String nume, int nrCont)
	{
		this.nume = nume;
		this.nrCont = nrCont;
	}
	
	public String getNume()
	{
		return this.nume;
	}
	
	public int getNrCont()
	{
		return this.nrCont;
	}
		
	public void setNume(String n_nume)
	{
		nume = n_nume;
	}
	
	public void setNrCont(int nr)
	{
		nrCont = nr;
	}

	@Override
	public void update(Observable arg0, Object arg1) 
	{
		System.out.print((String)arg1);
	}
	
}