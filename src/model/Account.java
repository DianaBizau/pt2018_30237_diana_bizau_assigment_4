package model;

import java.util.Observable;

public abstract class Account extends Observable
{
	protected Person p;
	protected int sold;
	protected int nrCont;
	protected String tip;
		
	public Account(Person n_p, int n_sold, int nnrCont, String n_tip)
	{
		p = n_p;
		sold = n_sold;
		nrCont = nnrCont;
		tip = n_tip;
	}
	
	public abstract Person getPerson();
	public abstract int getSold();
	public abstract String getTip();
	public abstract void extract();
		
}